# Node Noindex

This module lets the administrator set the HTML robots metatag to noindex for a
specific node. This will instruct well-behaved search engine robots to not 
index the node, preventing the node from appearing in search engine results. 
Note that the same functionality is not achieved by disallowing a node in 
robots.txt.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/node_noindex).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/node_noindex).

## Requirements

This module requires no modules outside of Drupal core.

## Installation

Install as you would normally install a contributed Drupal module. For further 
information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

1. Edit a node type and enable the _Show the 'Exclude from search engines' 
   field_ option in the _Node Noindex settings_ section.
2. When creating or editing a node, enable the _Exclude from search engines_
   option in the _Search engine settings_ section.

## Similar projects
- [Metatag](https://www.drupal.org/project/metatag): This module provides a 
  very comprehensive framework setting metatags. Use it if you require 
  capabilities beyond the scope of this project.
- [Noindex Module](https://www.drupal.org/project/noindex): This module adds 
  noindex tags to whitelisted paths.
